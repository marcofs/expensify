from django.forms import model_to_dict
from django.shortcuts import redirect, render

from app.models import UserClient, Client


def home(request):
    if not request.user.is_authenticated:
        return redirect('login')
    user_clients = UserClient.objects.filter(user=request.user.id).values()
    clients = []
    for user_client in user_clients:
        client = Client.objects.get(pk=user_client['client_id'])
        clients.append(model_to_dict(client))
    return render(request, 'home.html', {'clients': clients})