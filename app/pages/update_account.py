from django.shortcuts import render

from expensify.forms import UpdateAccountForm


def update_account(request):
    form = UpdateAccountForm(instance=request.user)
    return render(request, 'account/update.html', {'form': form})
