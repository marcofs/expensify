from django.db.models import Q
from django.shortcuts import render

from app.models import Client, UserClient
from expensify.forms import AddClientForm, AddLocationForm


def add_client(request):
    client_form = AddClientForm()
    user_clients = UserClient.objects.filter(Q(user=request.user))
    clients = Client.objects.filter(Q(userclient__in=user_clients))
    clients_names = []
    for idx, client in enumerate(clients):
        clients_names.append((idx+1, client.name))
    return render(request, 'account/clients.html', {'client_form': client_form})
