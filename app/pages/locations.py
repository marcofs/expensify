from django.db.models import Q
from django.shortcuts import render

from app.models import Client, UserClient, Location
from expensify.forms import AddLocationForm


def add_location(request):
    user_clients = UserClient.objects.filter(Q(user=request.user))
    clients = Client.objects.filter(Q(userclient__in=user_clients))
    locations = Location.objects.filter(Q(client__in=clients))
    clients_names = []
    for idx, client in enumerate(clients):
        clients_names.append((idx+1, client.name))
    location_form = AddLocationForm(clients=clients_names)
    return render(request, 'account/locations.html', {'location_form': location_form, 'locations': locations, 'clients': clients})
