import json
import os
import datetime

from django.conf import settings
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import PasswordChangeView
from django.db.models import Q
from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render, redirect

from random import randint

from django.urls import reverse_lazy

from app.models import UserClient, Client, Location, Observation
import xlsxwriter


class PasswordChangeView(PasswordChangeView):
    form_class = PasswordChangeForm
    success_url = reverse_lazy('account/password/success')



