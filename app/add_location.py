import json

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse

from app.models import Client, Location


@login_required
def add_location(request):
    try:
        body_unicode = request.body.decode('utf-8')
        data = json.loads(body_unicode)
        client = Client.objects.get(Q(user=request.user), Q(name=data['client']))
        locations = list(Location.objects.filter(Q(client__exact=client) & Q(address__exact=data['address'])).values())
        if len(locations) > 0:
            return JsonResponse({'message': f"{client.name} already has a location with this address.", 'result': 'error'}, status=409)
        location = Location(client=client, address=data['address'], distance=data['distance'])
        location.save()
        return JsonResponse({'message': f"Location added to {client.name}.", 'result': 'success'}, status=200)
    except:
        return JsonResponse({'message': "An error occurred.", 'result': 'error'}, status=500)
