import json

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse

from app.models import Client, Location


@login_required
def delete_location(request):
    try:
        body_unicode = request.body.decode('utf-8')
        data = json.loads(body_unicode)
        client = Client.objects.get(Q(user=request.user), Q(name=data['client']))
        Location.objects.filter(Q(client=client, address=data['address'])).delete()
        return JsonResponse({'message': f"Location deleted", 'result': 'success'}, status=200)
    except:
        return JsonResponse({'message': "An error occurred.", 'result': 'error'}, status=500)
