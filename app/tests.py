import json

from django.db.models import Q
from django.test import TestCase

from app.models import MyUser, Client


class Authenticate(TestCase):
    def setUp(self):
        self.user = MyUser.objects.create_user(username='testuser', password='12345', category=None, plate=None,
                                               nif=None, number=None)
        self.client.login(username='testuser', password='12345')


class AddClient(Authenticate):
    def setUp(self):
        super(AddClient, self).setUp()

    def test_add_valid_client(self):
        data = json.dumps({'name': "Foobar Ltd."})
        response = self.client.post('/add_client', data, content_type='application/json;charset=UTF-8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'message': "Client added.", 'result': 'success'}
        )
        list = Client.objects.filter(Q(user__exact=self.user))
        self.assertEqual(len(list), 1)
        self.assertEqual(list[0].name, "Foobar Ltd.")
        self.assertEqual(list[0].user, self.user)

