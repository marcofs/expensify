import json

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from app.models import MyUser


@login_required
def update_account(request):
    try:
        body_unicode = request.body.decode('utf-8')
        data = json.loads(body_unicode)
        user = MyUser.objects.get(username=request.user.username)
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.category = data['category']
        user.plate = data['plate']
        user.nif = data['nif']
        user.number = data['number']
        user.save()
        return JsonResponse({'message': 'Account details updated.', 'result': 'success'}, status=200)
    except:
        return JsonResponse({'message': 'An error occurred.', 'result': 'error'}, status=500)
