import json

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from app.models import Client, UserClient


@login_required
def add_client(request):
    try:
        body_unicode = request.body.decode('utf-8')
        data = json.loads(body_unicode)
        client = Client(user=request.user, name=data['name'])
        client.save()
        user_client = UserClient(user=request.user, client=client)
        user_client.save()
        return JsonResponse({'message': 'Client added.', 'result': 'success'}, status=200)
    except:
        return JsonResponse({'message': 'An error occurred.', 'result': 'error'}, status=500)
