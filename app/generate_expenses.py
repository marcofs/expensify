import json
import os
import datetime
from random import randint

import xlsxwriter
from django.db.models import Q
from django.http import JsonResponse

from app.models import Client, Location, Observation
from expensify import settings


def generate_expenses(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    user = request.user
    client = body['client']
    value = int(body['value'])

    wb = xlsxwriter.Workbook('expense.xlsx')
    worksheet = wb.add_worksheet(get_month_name(datetime.datetime.now().month))

    decimal_format = wb.add_format({'num_format': '#,##0.00'})
    decimal_format.set_font_size(8)
    decimal_format.set_font_name('Arial Narrow')

    holidays = {
        datetime.date(2022, 1, 1),
        datetime.date(2022, 4, 15),
        datetime.date(2022, 4, 17),
        datetime.date(2022, 4, 25),
        datetime.date(2022, 5, 1),
        datetime.date(2022, 6, 10),
        datetime.date(2022, 6, 16),
        datetime.date(2022, 8, 15),
        datetime.date(2022, 10, 5),
        datetime.date(2022, 11, 1),
        datetime.date(2022, 12, 1),
        datetime.date(2022, 12, 8),
        datetime.date(2022, 12, 25)
    }

    # Options
    worksheet.set_column_pixels(0, 999, 12)
    for i in range(100):
        worksheet.set_row_pixels(i, 17)
    worksheet.set_row_pixels(0, 24)
    worksheet.set_row_pixels(1, 19)
    worksheet.set_row_pixels(11, 6)
    worksheet.set_zoom(150)

    # Headers
    worksheet.write(0, 0, settings.COMPANY_NAME, get_options(wb, 'left', True, 'VAGRounded BT', 12))

    worksheet.merge_range('A2:AA2', settings.COMPANY_ADDRESS,
                          get_options(wb, 'left', False, 'Arial', 6))
    worksheet.merge_range('A3:AA3', settings.COMPANY_INFO, get_options(wb, 'left', False, 'Arial', 6))

    worksheet.merge_range('AZ1:CB1', "BOLETIM DE DESLOCAÇÕES",
                          get_options(wb, 'center', False, 'Copperplate Gothic Bold', 14))
    worksheet.merge_range('AZ2:CB2',
                          f"{get_month_name(datetime.datetime.now().month)[:3].upper()}/{str(datetime.datetime.now().year)[2:4]}",
                          get_options(wb, 'center', True, 'Copperplate Gothic Bold', 11))

    worksheet.merge_range('A5:AA5', "NOME", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('A6:AA6', f"{user.first_name} {user.last_name}",
                          get_options(wb, 'center', True, 'Arial Narrow', 12))

    worksheet.merge_range('AD5:AU5', "CATEGORIA", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('AD6:AU6', user.category, get_options(wb, 'center', True, 'Arial Narrow', 11))

    worksheet.merge_range('AX5:BA5', "Nº. PESSOAL", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('AX6:BA6', ' '.join([str(user.number)[i:i + 3] for i in range(0, len(str(user.number)), 3)]),
                          get_options(wb, 'center', True, 'Arial Narrow', 5))

    worksheet.merge_range('BD5:BJ5', "N.I.F.", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('BD6:BJ6', ' '.join([str(user.nif)[i:i + 3] for i in range(0, len(str(user.nif)), 3)]),
                          get_options(wb, 'center', True, 'Arial Narrow', 8))

    worksheet.merge_range('BM5:BS5', "", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('BM6:BS6', "", get_options(wb, 'center', True, 'Arial Narrow', 10))

    worksheet.merge_range('BV5:CB5', "VIATURA MATRÍCULA", get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range('BV6:CB6', user.plate, get_options(wb, 'center', True, 'Arial Narrow', 10))

    worksheet.merge_range('A8:CB8', settings.INFO_TOP, get_options(wb, 'center', False, 'Arial', 9))

    worksheet.merge_range('A10:C11', "DIA", get_options(wb, 'center', False, 'Arial Narrow', 7))
    worksheet.merge_range('D10:I10', "HORA", get_options(wb, 'center', False, 'Arial Narrow', 7))
    worksheet.merge_range('D11:F11', "PARTIDA", get_options(wb, 'center', False, 'Arial Narrow', 4))
    worksheet.merge_range('G11:I11', "REGRESSO", get_options(wb, 'center', False, 'Arial Narrow', 4))
    worksheet.merge_range('J10:Z11', "LOCAL/ITINERÁRIO", get_options(wb, 'center', False, 'Arial Narrow', 7))
    worksheet.merge_range('AA10:AN10', "AJUDAS DE CUSTO", get_options(wb, 'center', False, 'Arial Narrow', 7))
    worksheet.merge_range('AO10:AZ10', "KMS VIAT PROPRIA", get_options(wb, 'center', False, 'Arial Narrow', 7))
    worksheet.merge_range('AA11:AC11', "%", get_options(wb, 'center', False, 'Arial Narrow', 6))
    worksheet.merge_range('AD11:AE11', "Nº Dias", get_options(wb, 'center', False, 'Arial Narrow', 5))
    worksheet.merge_range('AF11:AI11', "ABONO DIÁRIO", get_options(wb, 'center', False, 'Arial Narrow', 4))
    worksheet.merge_range('AJ11:AN11', "A RECEBER", get_options(wb, 'center', False, 'Arial Narrow', 5))
    worksheet.merge_range('AO11:AR11', "KMS PREÇO", get_options(wb, 'center', False, 'Arial Narrow', 5))
    worksheet.merge_range('AS11:AU11', "PREÇO UNI.", get_options(wb, 'center', False, 'Arial Narrow', 4))
    worksheet.merge_range('AV11:AZ11', "A RECEBER", get_options(wb, 'center', False, 'Arial Narrow', 5))
    worksheet.merge_range('BA10:CB11', "OBSERVAÇÕES", get_options(wb, 'center', False, 'Arial Narrow', 7))

    # Day lines
    client = Client.objects.get(Q(user=request.user), Q(name=client))
    locations = list(Location.objects.filter(client=client))
    if len(locations) < 1:
        return JsonResponse({'message': f"{client.name} has no locations.", 'result': 'error'}, status=400)
    random_location = locations[randint(0, len(locations) - 1)]
    observations_len = Observation.objects.count()
    random_observation = Observation.objects.all()[randint(0, observations_len - 1)]

    aux = value / (random_location.distance * settings.PRICE_PER_KM)
    lines = round(aux)

    # Lower km until it matches value by ~2% diff
    new_km = random_location.distance
    allowed_offset = value * 0.02
    if value - (new_km * settings.PRICE_PER_KM * lines) > 0:
        while abs(value - (new_km * settings.PRICE_PER_KM * lines)) > allowed_offset:
            new_km += 1
    else:
        while abs(value - (new_km * settings.PRICE_PER_KM * lines)) > allowed_offset:
            new_km -= 1

    km_total = new_km * settings.PRICE_PER_KM * lines

    now = datetime.datetime.now()
    k = 0
    for i in range(1, 32):
        try:
            thisdate = datetime.date(now.year, now.month, i)
        except ValueError:
            break
        # Monday = 0, Sunday = 6
        if thisdate.weekday() < 5 and thisdate not in holidays:
            k += 1
            row = 13 + k - 1
            if k <= lines:
                worksheet.merge_range(f"A{row}:C{row}", i, get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"D{row}:F{row}", "09:00", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"G{row}:I{row}", "18:30", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"J{row}:Z{row}", client.name + " - " + random_location.address,
                                      get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AA{row}:AC{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AD{row}:AE{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AF{row}:AI{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AJ{row}:AN{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AO{row}:AR{row}", new_km, decimal_format)
                worksheet.merge_range(f"AS{row}:AU{row}", settings.PRICE_PER_KM,
                                      get_options_currency(wb, 'right', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"AV{row}:AZ{row}", new_km * settings.PRICE_PER_KM,
                                      get_options_currency(wb, 'right', False, 'Arial Narrow', 8))
                worksheet.merge_range(f"BA{row}:CB{row}", random_observation.description,
                                      get_options(wb, 'center', False, 'Arial Narrow', 8))

    # Add padding lines. Formatting stays in case of manual input.
    padding_lines = settings.DAY_LINES
    if padding_lines - lines < 0:
        padding_lines = k
    for i in range(padding_lines - lines):
        row = 13 + lines + i
        worksheet.merge_range(f"A{row}:C{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))
        worksheet.merge_range(f"D{row}:F{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"G{row}:I{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"J{row}:Z{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"AA{row}:AC{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"AD{row}:AE{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"AF{row}:AI{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"AJ{row}:AN{row}", "", get_options(wb, 'center', False, 'Arial Nari', 8))
        worksheet.merge_range(f"AO{row}:AR{row}", "", decimal_format)
        worksheet.merge_range(f"AS{row}:AU{row}", "", get_options_currency(wb, 'right', False, 'Arial Narrow', 8))
        worksheet.merge_range(f"AV{row}:AZ{row}", "", get_options_currency(wb, 'right', False, 'Arial Narrow', 8))
        worksheet.merge_range(f"BA{row}:CB{row}", "", get_options(wb, 'center', False, 'Arial Narrow', 8))

    # Totals
    worksheet.set_row_pixels(12 + padding_lines, 6)
    worksheet.merge_range(f"AD{13 + padding_lines + 1}:AI{13 + padding_lines + 1}", "TOTAL",
                          get_options(wb, 'center', False, 'Arial Narrow', 8))
    worksheet.merge_range(f"AJ{13 + padding_lines + 1}:AN{13 + padding_lines + 1}", "",
                          get_options(wb, 'center', True, 'Arial Narrow', 8))
    worksheet.merge_range(f"AO{13 + padding_lines + 1}:AU{13 + padding_lines + 1}", "TOTAL",
                          get_options(wb, 'center', False, 'Arial Narrow', 8))
    worksheet.merge_range(f"AV{13 + padding_lines + 1}:AZ{13 + padding_lines + 1}", km_total,
                          get_options_currency(wb, 'right', True, 'Arial Narrow', 8))
    worksheet.set_row_pixels(14 + padding_lines, 6)

    worksheet.merge_range(f"A{15 + padding_lines + 1}:AB{15 + padding_lines + 1}", "AUTORIZOU",
                          get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range(f"AD{15 + padding_lines + 1}:AJ{15 + padding_lines + 1}", "DATA",
                          get_options(wb, 'center', False, 'Arial', 5))
    worksheet.merge_range(f"BA{15 + padding_lines + 1}:CB{15 + padding_lines + 1}", "RECEBEU",
                          get_options(wb, 'center', False, 'Arial', 5))

    worksheet.set_row_pixels(15 + padding_lines, 12)
    worksheet.set_row_pixels(16 + padding_lines, 33)
    worksheet.merge_range(f"A{16 + padding_lines + 1}:AB{16 + padding_lines + 1}", "",
                          get_options(wb, 'center', False, 'Arial Narrow', 8))
    month = str(datetime.datetime.now().month)
    if len(month) == 1:
        month = month.zfill(1)
    date_last_day = f"{get_month_last_day(int(month))}/{month}/{datetime.datetime.now().year}"
    worksheet.merge_range(f"AD{16 + padding_lines + 1}:AJ{16 + padding_lines + 1}", date_last_day,
                          get_options(wb, 'center', False, 'Century Gothic', 10))
    worksheet.merge_range(f"BA{16 + padding_lines + 1}:CB{16 + padding_lines + 1}", km_total,
                          get_options_currency(wb, 'center', True, 'Arial Narrow', 18))

    worksheet.merge_range(f"A{19 + padding_lines + 1}:CB{19 + padding_lines + 1}", settings.INFO_BOTTOM,
                          get_options(wb, 'center', False, 'Arial', 9))

    wb.close()

    f = open('expense.xlsx', 'rb')
    data = f.read()
    f.close()
    os.remove('expense.xlsx')
    data = list(map(int, data))
    return JsonResponse({'file': data, 'message': "Excel sheet generated.", 'result': 'success'})


def get_options(workbook, align, bold, name, size):
    options = workbook.add_format({'align': align, 'valign': 'vcenter', 'bold': bold})
    options.set_font_name(name)
    options.set_font_size(size)
    return options


def get_options_currency(workbook, align, bold, name, size):
    options = workbook.add_format({'align': align, 'valign': 'vcenter', 'bold': bold, 'num_format': '#,##0.00 "€"'})
    options.set_font_name(name)
    options.set_font_size(size)
    return options


def get_month_name(number):
    months = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    ]
    return months[number - 1]


def get_month_last_day(number):
    last_days = [
        31,
        28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31
    ]
    return last_days[number - 1]