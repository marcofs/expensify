<p><img width=100 src="https://www.nearpartner.com/wp-content/uploads/2018/02/logo-positivo@2x.png" /></p>

# Expensify – A user guide
This document demonstrates the flow of generating an expense report on a new account.

## Log in
[expensify.nearpartner.com/login](https://expensify.nearpartner.com/login)

![](https://i.imgur.com/5te6DXz.png)

## Create a client
[expensify.nearpartner.com/account/clients](http://expensify.nearpartner.com/account/clients)

![](https://i.imgur.com/y19gvzO.png)

## Create a location
[expensify.nearpartner.com/account/locations](http://expensify.nearpartner.com/account/locations)

![](https://i.imgur.com/grC8Hff.png)

## Update your information
[expensify.nearpartner.com/account/update](http://expensify.nearpartner.com/account/update)

![](https://i.imgur.com/yGS9GhC.png)

## Generate an expense report
[https://expensify.nearpartner.com/](https://expensify.nearpartner.com/)

![](https://i.imgur.com/3jrGOLQ.png)

![](https://i.imgur.com/S0m9Ae5.png)

![](https://i.imgur.com/Jx6JGu8.png)

## Change your password
[expensify.nearpartner.com/account/password](http://expensify.nearpartner.com/account/password)

![](https://i.imgur.com/bSRQIpI.png)
