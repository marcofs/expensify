from django import forms
from django.forms import MultipleChoiceField, widgets

from app.models import MyUser, Client


class UpdateAccountForm(forms.ModelForm):
    first_name = forms.CharField(label='First name', max_length=50)
    last_name = forms.CharField(label='Last name', max_length=50)
    category = forms.CharField(label='Category', max_length=100)
    plate = forms.CharField(label='Plate', max_length=10)
    nif = forms.IntegerField(label='NIF')
    number = forms.CharField(label='Phone number', max_length=20, required=False)

    class Meta:
        model = MyUser
        fields = ('first_name', 'last_name', 'category', 'plate', 'nif', 'number')


class AddClientForm(forms.ModelForm):
    name = forms.CharField(label='Name', max_length=100)

    class Meta:
        model = Client
        fields = ('name',)


class AddLocationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        clients = kwargs.pop('clients')
        super(AddLocationForm, self).__init__(*args, **kwargs)

        self.fields['clients'].choices = clients

    clients = forms.ChoiceField(label='Client', choices=[])
    address = forms.CharField(label='Address', max_length=300)
    distance = forms.IntegerField(label='Distance')