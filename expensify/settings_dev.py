from expensify.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'expensify',
        'USER': 'postgres',
        'PASSWORD': 'marco',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}