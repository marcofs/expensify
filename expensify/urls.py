import django.contrib.auth.views
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from app import views
from app.generate_expenses import generate_expenses
from app.pages.home import home
from app.pages.update_account import update_account as get_update_account
from app.pages.clients import add_client as get_add_client
from app.pages.locations import add_location as get_add_location
from app.update_account import update_account
from app.add_client import add_client
from app.add_location import add_location
from app.delete_location import delete_location
from app.views import PasswordChangeView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', home, name='home'),
    path('generate_expenses', generate_expenses, name='generate_expenses'),
    path('account/update', get_update_account, name='account/update'),
    path('account/clients', get_add_client, name='account/clients'),
    path('account/locations', get_add_location, name='account/locations'),
    path('update_account', update_account, name='update_account'),
    path('add_client', add_client, name='add_client'),
    path('add_location', add_location, name='add_location'),
    path('delete_location', delete_location, name="delete_location"),
    path('account/password/success', TemplateView.as_view(template_name='registration/change_password_success.html'), name='account/password/success'),
    path('account/password', PasswordChangeView.as_view(template_name='registration/change_password.html'), name='account/password'),
    path('guide', TemplateView.as_view(template_name='guide.html'), name='guide'),
]