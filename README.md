<p>
<img src="https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=green" />
<img src="https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue" />
</p>

# Expensify
<img width="400" src="http://4.bp.blogspot.com/-kRCeA1TC0Qs/UBPqg8yO97I/AAAAAAAAXZ0/AqBQmNbDmbI/s1600/Scale1.jpg" />

## About
It is a common practice that, every month, many people need to take the time to fill a spreadsheet detailing their monthly expenses, usually reporting fictional trips to justify their allowance income. Expensify is a mock allowance expense generator that can help employees create their allowance reports in one click.

## Configuration
### Database
If you want to run a development database, edit `settings_dev.py` with your database information.
```
from expensify.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'YOUR DATABASE NAME HERE',
        'USER': 'YOUR USERNAME HERE',
        'PASSWORD': 'YOUR PASSWORD HERE',
        'HOST': 'YOUR HOST HERE',
        'PORT': 'YOUR PORT HERE',
    }
}
```

### Application
| Variable        	| Value                                              	|
|-----------------	|----------------------------------------------------	|
| PRICE_PER_KM    	| Price (€) per km                                   	|
| COMPANY_NAME    	| Name of the company (top-right title)              	|
| COMPANY_ADDRESS 	| Company address (text under the company's name)    	|
| COMPANY_INFO    	| Any information (text under the company's address) 	|
| INFO_TOP        	| Text/disclaimer on top of the table                	|
| INFO_BOTTOM     	| Text/disclaimer below the table                    	|
| DAY_LINES       	| Minimum number of lines in the table               	|

## Running
### Docker
- `git clone https://gitlab.com/marcofs/expensify`
- `cd expensify`
- `docker compose up`<br />
On a new terminal:
    - `docker exec -it expensify-db bash`
    - `su - postgres`
    - `psql`
    - `\password`
    - Set a new password
    - `\q`
    - `exit`
    - `exit`
    - Edit `DATABASES` > `default` > `PASSWORD` on `settings.py`
    - `docker exec -it expensify-web bash`
    - `python manage.py migrate`

From this moment onwards, all you need to do to start the application is `docker compose up`.

### Development
- `git clone https://github.com/marcofs/expensify`
- `cd expensify`
- `pip install -r requirements.txt`
- Edit `settings_dev.py` with your information
- `python manage.py migrate --settings=expensify.settings_dev`
- `python manage.py runserver <port> --settings=expensify.settings_dev`

### Testing
- `git clone https://github.com/marcofs/expensify`
- `cd expensify`
- `pip install -r requirements.txt`
- `python manage.py migrate --settings=expensify.settings_test`
- `python manage.py runserver <port> --settings=expensify.settings_test`
